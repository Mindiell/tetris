# encoding: utf-8

# Disable support prompt as we want a clean program
import os
os.environ["PYGAME_HIDE_SUPPORT_PROMPT"] = ""
import pygame

class Screen():
    def __init__(self, application, me=0):
        self.app = application
        self.me = me

    def run(self):
        # Render screen
        self.render()
        # Flip screen
        pygame.display.flip()
        # Update screen
        self.update()
        # Manage events
        return self.manage_events()

    def render(self):
        """
        Default rendering screen
        """
        # Display background
        self.app.screen.fill((120, 120, 120))

    def update(self):
        """
        Default update: do nothing
        """
        pass

    def manage_events(self):
        """
        Default events management:
        - Q key : quits application
        """
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return -1
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_q:
                    return -1
        return 0

    def open(self):
        """
        Opening screen: do nothing
        """
        pass

    def close(self):
        """
        Closing screen: do nothing
        """
        pass


class App():
    def __init__(self, title="", version=""):
        self.running = True
        self.timer = 0
        self.fps = 0
        self.max_fps = 60
        self.title = title
        self.version = version
        self.show_mouse = False
        self.full_screen = False
        self.width = 640
        self.height = 400
        self.screens = [Screen(self)]
        self.actual_screen = 0
        self.context = {}

    def init_game(self):
        """
        Default game initalization
        """
        # PyGame basic initialization
        pygame.init()
        pygame.display.set_caption("SmallGames - %s - v%s" % (
            self.title,
            self.version,
        ))
        logo = pygame.image.load("datas/smallgames.gif")
        pygame.display.set_icon(logo)
        self.init_screen()
        self.init_keyboard()

    def init_screen(self):
        """
        Default screen initialization
        """
        options = pygame.HWSURFACE | pygame.DOUBLEBUF
        if self.full_screen:
            options |= pygame.FULLSCREEN
        self.screen = pygame.display.set_mode(
            (self.width, self.height),
            options
        )

    def init_keyboard(self):
        """
        Default keyboard initialization
        """
        # Repeat pressed keys : (delay, interval)
        pygame.key.set_repeat(500, 100)
        # Hide mouse cursor if necessary
        pygame.mouse.set_visible(self.show_mouse)

    def run(self):
        """
        Default running loop
        """
        self.init_game()
        # Main loop
        clock = pygame.time.Clock()
        while(self.actual_screen>=0):
            clock.tick(self.max_fps)
            new_screen = self.screens[self.actual_screen].run()
            if new_screen != self.actual_screen:
                self.screens[self.actual_screen].close()
                self.actual_screen = new_screen
                self.screens[self.actual_screen].open()
