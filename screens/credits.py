# encoding: utf-8
"""
This screen simply displays credits.
"""

import pygame
from app import Screen
from tetris import SCREENS


class CreditsScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["CREDITS_SCREEN"])
        # Load background picture
        self.background = pygame.image.load("datas/credits.png").convert()

    def manage_events(self):
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE or event.key==pygame.K_q:
                    return SCREENS["MENU_SCREEN"]
        return self.me

    def render(self):
        # Render background
        self.app.screen.blit(self.background, (0, 0))
