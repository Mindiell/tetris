# encoding: utf-8
"""
This screen simply displays main menu.
"""

import pygame
import pygame.font
from app import Screen
from tetris import SCREENS


class MenuScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["MENU_SCREEN"])
        # TEST
        self.test = pygame.image.load("datas/nuskool_krome_64x64.png").convert()
        self.caractere = 0

        # Load font for menus
        self.font = pygame.font.SysFont("Verdana", 32)
        self.menu_texts = (
            "Play",
            "High Scores",
            "Credits",
            "Quit",
        )
        # Prepare menus
        self.menus = [
            self.font.render(
                menu_text,
                False,
                (255,255,255)
            ) for menu_text in self.menu_texts
        ]
        # Selected menu is red
        self.selected_menu = 0
        self.menus[self.selected_menu] = self.font.render(
            self.menu_texts[self.selected_menu],
            False,
            (255,0,0)
        )

    def manage_events(self):
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE:
                    return SCREENS["NO_SCREEN"]
                elif event.key==pygame.K_LEFT:
                    self.caractere -= 1
                    print(self.caractere)
                elif event.key==pygame.K_RIGHT:
                    self.caractere += 1
                    print(self.caractere)
                elif event.key==pygame.K_DOWN:
                    # Old selected menu is white again
                    self.menus[self.selected_menu] = self.font.render(
                        self.menu_texts[self.selected_menu],
                        False,
                        (255,255,255)
                    )
                    self.selected_menu = (self.selected_menu+1) % len(self.menu_texts)
                    # New selected menu is red
                    self.menus[self.selected_menu] = self.font.render(
                        self.menu_texts[self.selected_menu],
                        False,
                        (255,0,0)
                    )
                elif event.key==pygame.K_UP:
                    # Old selected menu is white again
                    self.menus[self.selected_menu] = self.font.render(
                        self.menu_texts[self.selected_menu],
                        False,
                        (255,255,255)
                    )
                    self.selected_menu = (self.selected_menu-1) % len(self.menu_texts)
                    # New selected menu is red
                    self.menus[self.selected_menu] = self.font.render(
                        self.menu_texts[self.selected_menu],
                        False,
                        (255,0,0)
                    )
                elif event.key==pygame.K_RETURN:
                    # Activate menu
                    if self.selected_menu+1 < len(self.menu_texts):
                        return self.selected_menu+1
                    else:
                        return SCREENS["NO_SCREEN"]
        return self.me

    def render(self):
        # Erase screen
        self.app.screen.fill((0, 0, 0))
        # Display each menu
        height = 30
        for menu in self.menus:
            self.app.screen.blit(
                menu,
                (
                    self.app.width/2 - menu.get_width()/2,
                    height
                )
            )
            height += 20 + menu.get_height()
        # TEST
        """ !"#"""
        self.app.screen.blit(
            self.test,
            (0, 0),
            pygame.Rect(self.caractere*64,0,64,64)
        )

