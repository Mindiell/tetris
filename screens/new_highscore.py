# encoding: utf-8
"""
This screen saves new high-score.
"""

import json
import pygame
from app import Screen
from tetris import SCREENS


class NewHighScoreScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["NEW_HIGH_SCORE_SCREEN"])
        # Saving a highscore
        self.saving_highscore = True
        # Loading scores file
        with open("scores.json") as f:
            self.scores = json.load(f)
        # Set font
        self.font = pygame.font.SysFont("Times New Roman", 24)
        self.high_score = False
        self.name = ""

    def open(self):
        self.high_score = False
        if self.app.score > self.scores[7][1]:
            self.high_score = True
            self.name = ""

    def manage_events(self):
        if not self.high_score:
            return SCREENS["MENU_SCREEN"]
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type==pygame.KEYDOWN:
                # Manage keys to set highscore name
                if (
                    pygame.K_a <= event.key <= pygame.K_z
                ) or (
                    event.key == pygame.K_SPACE
                ) or (
                    pygame.K_0 <= event.key <= pygame.K_9
                ):
                    if len(self.name)<8:
                        self.name += chr(event.key)
                elif event.key == pygame.K_BACKSPACE:
                    if len(self.name)>0:
                        self.name = self.name[:-1]
                elif event.key == pygame.K_RETURN:
                    self.save_score()
                    return SCREENS["HIGH_SCORES_SCREEN"]
        return self.me

    def render(self):
        self.app.screen.fill((0, 0, 0))
        # Render name and score
        score_name = self.font.render(
            "%s" % self.name,
            True,
            (255,255,255)
        )
        score_value = self.font.render(
            "%d" % self.app.score,
            True,
            (255,255,255)
        )
        self.app.screen.blit(
            score_name,
            (
                180,
                50
            )
        )
        self.app.screen.blit(
            score_value,
            (
                440 - score_value.get_width(),
                50
            )
        )

    def save_score(self):
        idx = 9
        for index in range(8):
            if self.scores[index][1] < self.app.score:
                idx = index
                break
        if idx<8:
            # Copy and erase old highscores
            for index in range(7, idx, -1):
                self.scores[index] = self.scores[index-1]
            # Insert new highscore
            self.scores[idx] = [self.name, self.app.score]
        with open("scores.json", "w") as f:
            json.dump(self.scores, f)
