# encoding: utf-8
"""
This screen is the core game itself.
"""

import pygame
import pygame.font
import random
import time
from app import Screen
from tetris import SCREENS


class GameScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["GAME_SCREEN"])
        # Board game
        self.board = []
        self.board_height = 24
        self.board_max_height = self.board_height + 3
        self.board_width = 9
        self.board_max_width = self.board_width + 2
        # Create the board itself
        for height in range(self.board_max_height):
            self.board.append([])
            for width in range(self.board_max_width):
                self.board[height].append(0)
        # Add left, right and bottom borders in order to block squares
        for height in range(self.board_max_height):
            self.board[height][0] = 255
            self.board[height][self.board_max_width-1] = 255
        for width in range(self.board_max_width):
            self.board[self.board_max_height-1][width] = 255
        # Load squares pictures
        self.squares = {
            i: pygame.image.load(
                "datas/%d.png" % i
            ).convert()
            for i in range(1,8)
        }
        # Load background picture
        self.background = pygame.image.load("datas/background.png").convert()
        # Set font
        self.font = pygame.font.SysFont("Times New Roman", 24)
        # Set lines
        self.lines = 0
        self.lines_width = len(str(self.lines))
        self.render_lines()
        # Set score
        self.score = 0
        self.score_width = len(str(self.score))
        self.render_score()
        # Set level
        self.level = 1
        self.level_width = len(str(self.level))
        self.level_text = self.font.render(
            "%d" % self.level,
            True,
            (255,255,255)
        )
        # Set default game's values
        self.rotation = 0
        self.next_brick = random.randint(1,7)
        self.brick_stats = [0,0,0,0,0,0,0]
        self.brick_stats[self.next_brick-1] += 1
        self.display_next_piece = False
        # Set timing values
        self.timer = time.perf_counter()
        self.speed = 1
        # Set game over
        font = pygame.font.SysFont("Times New Roman", 48)
        self.game_over = False
        self.game_over_text = font.render(
            "GAME OVER",
            True,
            (255,0,0)
        )

    def open(self):
        pygame.mixer.music.load("datas/theme.ogg")
        pygame.mixer.music.set_volume(.5)
        pygame.mixer.music.play(loops=-1)
        # Reset board
        for height in range(self.board_max_height):
            for width in range(self.board_max_width):
                if self.board[height][width] < 255:
                    self.board[height][width] = 0
        # Reset lines
        self.lines = 0
        self.render_lines()
        # Reset level
        self.level = 1
        self.render_level()
        # Reset score
        self.score = 0
        self.render_score()
        # Reset game over
        self.game_over = False
        # Create a new brick
        self.brick_create()

    def close(self):
        pygame.mixer.music.stop()
        self.app.score = self.score

    def render(self):
        # Render background
        self.app.screen.blit(self.background, (0, 0))
        # Render score
        self.app.screen.blit(
            self.score_text,
            (
                560 - self.score_width * 8,
                12
            )
        )
        # Render lines
        self.app.screen.blit(
            self.lines_text,
            (
                560 - self.lines_width * 8,
                160
            )
        )
        # Render level
        self.app.screen.blit(
            self.level_text,
            (
                560 - self.level_width * 8,
                120
            )
        )
        # Render board
        offset = 16
        for height in range(self.board_max_height):
            pos_y = height * offset - 32
            for width in range(self.board_max_width):
                pos_x = 232 + width * offset
                if 0 < self.board[height][width] < 10:
                    self.app.screen.blit(
                        self.squares[self.board[height][width]],
                        (pos_x, pos_y)
                    )
                elif 10 < self.board[height][width] < 20:
                    self.app.screen.blit(
                        self.squares[self.board[height][width]-10],
                        (pos_x, pos_y)
                    )
        # Render next piece
        if self.display_next_piece:
            pass
        # Render game over
        if self.game_over:
            self.app.screen.blit(
                self.game_over_text,
                (
                    self.app.screen.get_width()/2 - self.game_over_text.get_width()/2,
                    self.app.screen.get_height()/2 - self.game_over_text.get_height()/2
                )
            )
            

    def render_score(self):
        self.score_text = self.font.render(
            "%d" % self.score,
            True,
            (255,255,255)
        )

    def render_lines(self):
        self.lines_text = self.font.render(
            "%d" % self.lines,
            True,
            (255,255,255)
        )

    def render_level(self):
        self.level_text = self.font.render(
            "%d" % self.level,
            True,
            (255,255,255)
        )

    def update(self):
        if self.game_over:
            return
        if self.timer + self.speed < time.perf_counter():
            self.timer = time.perf_counter()
            self.brick_down()

    def manage_events(self):
        if self.game_over:
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    return SCREENS["NEW_HIGH_SCORE_SCREEN"]
            return self.me
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    return SCREENS["MENU_SCREEN"]
                elif event.key == pygame.K_j:#K_LEFT:
                    self.brick_left()
                elif event.key == pygame.K_l:#K_RIGHT:
                    self.brick_right()
                elif event.key == pygame.K_k:#K_DOWN:
                    self.brick_down()
                elif event.key == pygame.K_i:#K_UP:
                    self.brick_rotate()
                elif event.key == pygame.K_SPACE:
                    while self.brick_down():
                        pass
                elif event.key == pygame.K_n:
                    self.toggle_next_piece()
        return self.me

    def toggle_next_piece(self):
        self.display_next_piece = not self.display_next_piece

    def board_falling(self):
        lines = 0
        # Verifying any visible line is complete
        height = self.board_max_height-2
        while height != 0:
            full_line = True
            for width in range(self.board_max_width):
                if self.board[height][width]<10:
                    full_line = False
                    break
            if full_line:
                # This line is full and should disappear
                lines += 1
                # Move squares down, apart special squares
                for upper in range(height, 0, -1):
                    for width in range(1, self.board_max_width-1):
                        self.board[upper][width] = self.board[upper-1][width]
                # Empty first line
                for width in range(1, self.board_max_width-1):
                    self.board[0][width] = 0
                # Retest this line
                height += 1
            height -= 1
        # Update score based on number of lines completed
        if lines > 0:
            # Refresh lines
            self.lines += lines
            self.render_lines()
            # Update score
            self.score += self.level * 4**lines

    def brick_create(self):
        if self.next_brick == 1:
            # square
            self.board[0][5] = self.next_brick
            self.board[0][6] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[1][6] = self.next_brick
        elif self.next_brick == 2:
            # line
            self.board[1][4] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[1][6] = self.next_brick
            self.board[1][7] = self.next_brick
        elif self.next_brick == 3:
            # L form
            self.board[0][4] = self.next_brick
            self.board[1][4] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[1][6] = self.next_brick
        elif self.next_brick == 4:
            # L form inverted
            self.board[1][4] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[1][6] = self.next_brick
            self.board[0][6] = self.next_brick
        elif self.next_brick == 5:
            # T form
            self.board[1][4] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[0][5] = self.next_brick
            self.board[1][6] = self.next_brick
        elif self.next_brick == 6:
            # S form
            self.board[1][4] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[0][5] = self.next_brick
            self.board[0][6] = self.next_brick
        elif self.next_brick == 7:
            # S form inverted
            self.board[0][4] = self.next_brick
            self.board[0][5] = self.next_brick
            self.board[1][5] = self.next_brick
            self.board[1][6] = self.next_brick
        # Compute next brick
        self.next_brick = random.randint(1,7)
        self.brick_stats[self.next_brick-1] += 1
        # Reset rotation
        self.rotation = 0

    def brick_left(self):
        """
        Trying to move brick on left
        """
        for height in range(self.board_max_height):
            for width in range(1, self.board_max_width):
                if (
                    0 < self.board[height][width] < 10
                ) and (
                    self.board[height][width] < self.board[height][width-1]
                ):
                    return
        for height in range(self.board_max_height):
            for width in range(1, self.board_max_width):
                if 0 < self.board[height][width] < 10:
                    self.board[height][width-1] = self.board[height][width]
                    self.board[height][width] = 0

    def brick_right(self):
        """
        Trying to move brick on right
        """
        for height in range(self.board_max_height):
            for width in range(self.board_max_width, 0, -1):
                if (
                    0 < self.board[height][width-1] < 10
                ) and (
                    self.board[height][width-1] < self.board[height][width]
                ):
                    return
        for height in range(self.board_max_height):
            for width in range(self.board_max_width, 0, -1):
                if 0 < self.board[height][width-1] < 10:
                    self.board[height][width] = self.board[height][width-1]
                    self.board[height][width-1] = 0

    def brick_down(self):
        """
        Make the actual brick to fall one line at a time,
        blocking it if bottom reached.
        """
        permitted = True
        # Search for a brick to move in all the board
        for height in range(self.board_max_height, 0, -1):
            for width in range(self.board_max_width):
                # If board contains an integer lesser than 10
                # and a space is available under
                if (
                    0 < self.board[height-1][width] < 10
                ) and (
                    self.board[height-1][width] < self.board[height][width]
                ):
                    permitted = False
                    break
        if permitted:
            for height in range(self.board_max_height, 0, -1):
                for width in range(self.board_max_width):
                    if 0 < self.board[height-1][width] < 10:
                        self.board[height][width] = self.board[height-1][width]
                        self.board[height-1][width] = 0
            return True
        # Freeze brick
        for height in range(self.board_max_height):
            for width in range(self.board_max_width):
                if 0 < self.board[height][width] < 10:
                    self.board[height][width] += 10
        # Add some score
        self.score += self.level
        # Is line completed ?
        self.board_falling()
        # If number of lines changed, do we pass to next level ?
        if self.lines > self.level * 10 and self.level < 10:
            self.level += 1
            # Refresh level
            self.render_level()
            # Modify game speed
            self.speed = 1-(self.level/12)
        # Refresh score
        self.render_score()
        # Create a new brick
        self.brick_create()
        # Verifying first and second lines if a brick is freezed
        for width in range(1, self.board_max_width-1):
            if self.board[0][width] > 10 or self.board[1][width] > 10:
                self.game_over = True
                pygame.mixer.music.stop()
        return False

    def brick_rotate(self):
        for height in range(self.board_max_height):
            for width in range(self.board_max_width):
                if 0 < self.board[height][width] < 10:
                    brick = self.board[height][width]
                    # Brick #1 is perfect square so does not need to rotate
                    if brick == 2:
                        # flat line
                        if self.rotation%2 == 0:
                            if (
                                self.board[height+2][width+2] == 0 and
                                self.board[height+1][width+2] == 0 and
                                self.board[height-1][width+2] == 0
                            ):
                                self.board[height+2][width+2] = brick
                                self.board[height+1][width+2] = brick
                                self.board[height-1][width+2] = brick
                                self.board[height][width] = 0
                                self.board[height][width+1] = 0
                                self.board[height][width+3] = 0
                                self.rotation = (self.rotation+1)%4
                        elif (
                            self.board[height+2][width-2] == 0 and
                            self.board[height+2][width-1] == 0 and
                            self.board[height+2][width+1] == 0
                        ):
                            self.board[height+2][width-2] = brick
                            self.board[height+2][width-1] = brick
                            self.board[height+2][width+1] = brick
                            self.board[height][width] = 0
                            self.board[height+1][width] = 0
                            self.board[height+3][width] = 0
                            self.rotation = (self.rotation+1)%4
                        return
                    elif brick == 3:
                        # L form
                        if self.rotation == 0:
                            if (
                                self.board[height][width+1] == 0 and
                                self.board[height-1][width+1] == 0
                            ):
                                self.board[height][width+1] = brick
                                self.board[height-1][width+1] = brick
                                self.board[height][width] = 0
                                self.board[height+1][width+2] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 1:
                            if (
                                self.board[height+1][width-2] == 0 and
                                self.board[height+1][width-1] == 0
                            ):
                                self.board[height+1][width-2] = brick
                                self.board[height+1][width-1] = brick
                                self.board[height][width] = 0
                                self.board[height+2][width-1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 2:
                            if (
                                self.board[height+1][width+1] == 0 and
                                self.board[height+2][width+1] == 0
                            ):
                                self.board[height+1][width+1] = brick
                                self.board[height+2][width+1] = brick
                                self.board[height][width] = 0
                                self.board[height+1][width+2] = 0
                                self.rotation = (self.rotation+1)%4
                        elif (
                            self.board[height+1][width+1] == 0 and
                            self.board[height+1][width+2] == 0
                        ):
                            self.board[height+1][width+1] = brick
                            self.board[height+1][width+2] = brick
                            self.board[height][width+1] = 0
                            self.board[height+2][width] = 0
                            self.rotation = (self.rotation+1)%4
                        return
                    elif brick == 4:
                        # L form inverted
                        if self.rotation == 0:
                            if (
                                self.board[height][width-1] == 0 and
                                self.board[height+2][width] == 0
                            ):
                                self.board[height][width-1] = brick
                                self.board[height+2][width] = brick
                                self.board[height+1][width-2] = 0
                                self.board[height+1][width-1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 1:
                            if (
                                self.board[height+1][width] == 0 and
                                self.board[height][width+2] == 0
                            ):
                                self.board[height+1][width] = brick
                                self.board[height][width+2] = brick
                                self.board[height+1][width+1] = 0
                                self.board[height+2][width+1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 2:
                            if (
                                self.board[height-1][width] == 0 and
                                self.board[height+1][width+1] == 0
                            ):
                                self.board[height-1][width] = brick
                                self.board[height+1][width+1] = brick
                                self.board[height][width+1] = 0
                                self.board[height][width+2] = 0
                                self.rotation = (self.rotation+1)%4
                        elif (
                            self.board[height+1][width+1] == 0 and
                            self.board[height+2][width-1] == 0
                        ):
                            self.board[height+1][width+1] = brick
                            self.board[height+2][width-1] = brick
                            self.board[height][width] = 0
                            self.board[height+1][width] = 0
                            self.rotation = (self.rotation+1)%4
                        return
                    elif brick == 5:
                        # T form
                        if self.rotation == 0:
                            if self.board[height+2][width] == 0:
                                self.board[height+2][width] = brick
                                self.board[height+1][width+1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 1:
                            if self.board[height+1][width+1] == 0:
                                self.board[height+1][width+1] = brick
                                self.board[height][width] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 2:
                            if self.board[height-1][width+1] == 0:
                                self.board[height-1][width+1] = brick
                                self.board[height][width] = 0
                                self.rotation = (self.rotation+1)%4
                        elif self.rotation == 3:
                            if self.board[height+1][width-1] == 0:
                                self.board[height+1][width-1] = brick
                                self.board[height+2][width] = 0
                                self.rotation = (self.rotation+1)%4
                        return
                    elif brick == 6:
                        # S form
                        if self.rotation%2 == 0:
                            if self.board[height][width-1]==0 and self.board[height+2][width]==0:
                                self.board[height][width-1] = brick
                                self.board[height+2][width] = brick
                                self.board[height][width] = 0
                                self.board[height][width+1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif (
                            self.board[height][width+1] == 0 and
                            self.board[height][width+2] == 0
                        ):
                            self.board[height][width+1] = brick
                            self.board[height][width+2] = brick
                            self.board[height][width] = 0
                            self.board[height+2][width+1] = 0
                            self.rotation = (self.rotation+1)%4
                        return
                    elif brick == 7:
                        # S form inverted
                        if self.rotation%2 == 0:
                            if (
                                self.board[height][width+2] == 0 and
                                self.board[height+2][width+1] == 0
                            ):
                                self.board[height][width+2] = brick
                                self.board[height+2][width+1] = brick
                                self.board[height][width] = 0
                                self.board[height][width+1] = 0
                                self.rotation = (self.rotation+1)%4
                        elif (
                            self.board[height][width-1] == 0 and
                            self.board[height][width-2] == 0
                        ):
                            self.board[height][width-1] = brick
                            self.board[height][width-2] = brick
                            self.board[height][width] = 0
                            self.board[height+2][width-1] = 0
                            self.rotation = (self.rotation+1)%4
                        return
