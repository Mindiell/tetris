# encoding: utf-8
"""
This screen simply displays high scores.
"""

import json
import pygame
from app import Screen
from tetris import SCREENS


class HighScoresScreen(Screen):
    def __init__(self, application):
        super().__init__(application, SCREENS["HIGH_SCORES_SCREEN"])
        # Loading scores file
        try:
            with open("scores.json") as f:
                self.scores = json.load(f)
        except:
            # Creating default scores file
            self.scores = [
                ("this", 9999),
                ("game", 7000),
                ("is", 6000),
                ("funny!", 5000),
                ("me", 4000),
                ("you", 3000),
                ("every1", 2000),
                ("nobody", 1000),
            ]
            with open("scores.json", "w") as f:
                json.dump(self.scores, f)
        # Set font
        self.font = pygame.font.SysFont("Times New Roman", 24)

    def open(self):
        # Update highscores
        with open("scores.json") as f:
            self.scores = json.load(f)

    def manage_events(self):
        for event in pygame.event.get():
            if event.type==pygame.QUIT:
                return SCREENS["NO_SCREEN"]
            elif event.type==pygame.KEYDOWN:
                if event.key==pygame.K_ESCAPE or event.key==pygame.K_q:
                    return SCREENS["MENU_SCREEN"]
        return self.me

    def render(self):
        self.app.screen.fill((0, 0, 0))
        # Render each score separately
        for index in range(8):
            score_name = self.font.render(
                "%s" % self.scores[index][0],
                True,
                (255,255,255)
            )
            score_value = self.font.render(
                "%d" % self.scores[index][1],
                True,
                (255,255,255)
            )
            self.app.screen.blit(
                score_name,
                (
                    180,
                    50 + index*32
                )
            )
            self.app.screen.blit(
                score_value,
                (
                    440 - score_value.get_width(),
                    50 + index*32
                )
            )
