from screens.credits import CreditsScreen
from screens.game import GameScreen
from screens.highscores import HighScoresScreen
from screens.menu import MenuScreen
from screens.new_highscore import NewHighScoreScreen
