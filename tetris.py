# encoding: utf-8
"""
This game is a basic tetris game.

TODO:
 * Change level based on number of lines (x10)
 * Give the possibility to display next piece
 * Add self police in order to display beautiful text
 * Change game background
 * Add sounds when filling lines
 * Accelerate music based on level ?
 * Options :
   * Set own keys for playing
   * Authorize change rotate direction (clockwise and counter-clockwise) ?
   * Choose music ?
   * Set starting level ?
   * Other modes ?
     * Challenge mode : impossible to rotate pieces, new pieces are randomly choosen and rotated
     * Other modes ?
  * Add brick statistics
"""

from app import App
from screens import *

SCREENS = {
    "NO_SCREEN": -1,
    "MENU_SCREEN": 0,
    "GAME_SCREEN": 1,
    "HIGH_SCORES_SCREEN": 2,
    "CREDITS_SCREEN": 3,
    "NEW_HIGH_SCORE_SCREEN": 4,
}


class Game(App):
    def __init__(self):
        super().__init__("Tetris", "0.1")
        self.score = 0

    def init_game(self):
        super().init_game()
        self.screens = [
            MenuScreen(self),
            GameScreen(self),
            HighScoresScreen(self),
            CreditsScreen(self),
            NewHighScoreScreen(self),
        ]


if __name__ == "__main__":
    Game().run()
